<?php
#ini_set('pcre.backtrack_limit', 1000000);

define('_SPIP_TOPNAV', true);


// cache plus gros et plus long
$GLOBALS['quota_cache'] = 150;
define('_DUREE_CACHE_DEFAUT', 24*3600*30);

// lien vers le redmine...
// pour extensions code du plugin porte plume (pp_codes)
// redmine SVN (branche 3.0)
@define('_URL_BROWSER_TRAC',
        'https://core.spip.net/projects/spip/repository/entry/branches/spip-3.0/@file@');

define('CODES_SPIP_BRANCHE', '3.0');

// tris dans l'espace prive
define('_TRI_GROUPES_MOTS', 'multi');  # 0+titre,titre // multi
#define('_TRI_ARTICLES_RUBRIQUE', '0+titre,titre');  # date DESC
