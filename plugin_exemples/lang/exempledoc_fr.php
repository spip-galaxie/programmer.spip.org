<?php
$GLOBALS[$GLOBALS['idx_lang']] = array(
	// C
	'champ_obligatoire' => 'Ce champ est obligatoire',
	// E
	'erreur_calcul' => 'Erreur lors du calcul du quanti&egrave;me !',
	'erreur_saisie' => 'Votre saisie contient des erreurs !',
	// I
	'info_quantieme_calcule' => 'Quanti&egrave;me calcul&eacute; : @quantieme@',
	// L
	'label_bouton_calculer' => 'Trouver',
	'label_date' => 'Date (jj/mm/aaaa) :',
	'langue_du_visiteur' => 'Votre langue :',
	// R
	'retour_calcul' => 'Le quanti&egrave;me de @date_jour@ est @quantieme@',
	
);
?>
