<?php

if (!defined("_ECRIRE_INC_VERSION")) return;

/**
 * On définit le critère enfants si on n'a pas polyhiérarchie
 */
if(!function_exists('critere_enfants_dist')){
	function critere_enfants_dist($idb, &$boucles, $crit){
		global $exceptions_des_tables;
		$boucle = &$boucles[$idb];
	
		if (isset($crit->param[0])){
			$arg = calculer_liste($crit->param[0], array(), $boucles, $boucles[$idb]->id_parent);
		}
		else
			$arg = kwote(calculer_argument_precedent($idb, 'id_rubrique', $boucles));
	
		if ($boucle->type_requete == 'rubriques' OR isset($exceptions_des_tables[$boucle->id_table]['id_parent'])) {
			$id_parent = isset($exceptions_des_tables[$boucle->id_table]['id_parent']) ?
				$exceptions_des_tables[$boucle->id_table]['id_parent'] :
				'id_parent';
			$mparent = $boucle->id_table . '.' . $id_parent;
		}
		else {
			$mparent = $boucle->id_table . '.id_rubrique';
		}
	
		$where = array();
	
		$where[] = "is_array(\$r=$arg)?sql_in('$mparent',\$r):array('=', '$mparent', \$r)";

		$where = reset($where);
	
		$boucle->where[]= $where;
		
	}
}

/**
 * On définit le filtre me si on n'a pas forums
 */
if(!function_exists('filtre_me_dist')){
	function filtre_me_dist($id_objet, $objet, $id_auteur, $sioui=' ', $sinon=''){
		static $auteurs = array();
		if (!isset($auters[$objet][$id_objet])) {
			$r = sql_allfetsel("id_auteur","spip_auteurs_liens","objet=".sql_quote($objet)." AND id_objet=".intval($id_objet));
			$auteurs[$objet][$id_objet] = array_map('reset',$r);
		}
		return (in_array($id_auteur,$auteurs[$objet][$id_objet]))?$sioui:$sinon;
	}
}


?>
