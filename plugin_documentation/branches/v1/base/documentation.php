<?php
if (!defined("_ECRIRE_INC_VERSION")) return;

function documentation_declarer_champs_extras($champs = array()){
	$config_extras = lire_config('documentation/utiliser_champs_extras',array());
	if(!in_array('exemple',$config_extras)){
		$champs[] = new ChampExtra(array(
			'table' => 'article', // sur quelle table ?
			'champ' => 'exemple', // nom sql
			'label' => 'documentation:label_exemple', // chaine de langue 'prefix:cle'
			'type' => 'textarea', // type de saisie
			'sql' => "text NOT NULL DEFAULT ''", // declaration sql
			'traitements' => _TRAITEMENT_RACCOURCIS, // _TRAITEMENT_TYPO
			'rechercher' => 2,
	
			// experimental
			'saisie_externe' => true,
			'saisie_parametres' => array(
				'class' => "inserer_barre_edition inserer_previsualisation", // classes CSS
				'li_class' => "haut", // classes CSS
			),
		));
	}
	if(!in_array('exercice',$config_extras)){
		$champs[] = new ChampExtra(array(
			'table' => 'article', // sur quelle table ?
			'champ' => 'exercice', // nom sql
			'label' => 'documentation:label_exercice', // chaine de langue 'prefix:cle'
			'type' => 'textarea', // type de saisie
			'sql' => "text NOT NULL DEFAULT ''", // declaration sql
			'traitements' => _TRAITEMENT_RACCOURCIS, // _TRAITEMENT_TYPO
			'rechercher' => 2,
	
			// experimental
			'saisie_externe' => true,
			'saisie_parametres' => array(
				'class' => "inserer_barre_edition inserer_previsualisation", // classes CSS
				'li_class' => "haut", // classes CSS
			),
		));
	}
	if(!in_array('reponse',$config_extras)){
		$champs[] = new ChampExtra(array(
			'table' => 'article', // sur quelle table ?
			'champ' => 'reponse', // nom sql
			'label' => 'documentation:label_reponse', // chaine de langue 'prefix:cle'
			'type' => 'textarea', // type de saisie
			'sql' => "text NOT NULL DEFAULT ''", // declaration sql
			'traitements' => _TRAITEMENT_RACCOURCIS, // _TRAITEMENT_TYPO
			'rechercher' => 2,
	
			// experimental
			'saisie_externe' => true,
			'saisie_parametres' => array(
				'class' => "inserer_barre_edition inserer_previsualisation", // classes CSS
				'li_class' => "haut", // classes CSS
			),
		));
	}
	
	return $champs;
}
?>
