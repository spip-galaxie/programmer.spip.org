<?php
if (!defined("_ECRIRE_INC_VERSION")) return;

include_spip('inc/cextras_gerer');
include_spip('base/documentation');
	
function documentation_upgrade($nom_meta_base_version,$version_cible){
	$champs = documentation_declarer_champs_extras();
	installer_champs_extras($champs, $nom_meta_base_version, $version_cible);
}

function documentation_vider_tables($nom_meta_base_version) {
	$champs = documentation_declarer_champs_extras();
	desinstaller_champs_extras($champs, $nom_meta_base_version);
}
?>
