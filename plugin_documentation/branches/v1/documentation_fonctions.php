<?php

if (!defined("_ECRIRE_INC_VERSION")) return;

/**
 * On définit le critères enfants si on n'a pas polyhiérarchie
 */
if(!function_exists('critere_enfants_dist')){
	function critere_enfants_dist($idb, &$boucles, $crit){
		global $exceptions_des_tables;
		$boucle = &$boucles[$idb];
	
		if (isset($crit->param[0])){
			$arg = calculer_liste($crit->param[0], array(), $boucles, $boucles[$idb]->id_parent);
		}
		else
			$arg = kwote(calculer_argument_precedent($idb, 'id_rubrique', $boucles));
	
		if ($boucle->type_requete == 'rubriques' OR isset($exceptions_des_tables[$boucle->id_table]['id_parent'])) {
			$id_parent = isset($exceptions_des_tables[$boucle->id_table]['id_parent']) ?
				$exceptions_des_tables[$boucle->id_table]['id_parent'] :
				'id_parent';
			$mparent = $boucle->id_table . '.' . $id_parent;
		}
		else {
			$mparent = $boucle->id_table . '.id_rubrique';
		}
	
		$where = array();
	
		$where[] = "is_array(\$r=$arg)?sql_in('$mparent',\$r):array('=', '$mparent', \$r)";

		$where = reset($where);
	
		$boucle->where[]= $where;
		
	}
}

?>